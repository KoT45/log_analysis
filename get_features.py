from time import mktime
from datetime import datetime
import sys
import re
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


class Location:
    def __init__(self):
        self.location_id = ""
        self.average_speed = -1
        self.blocks_bytes_time_list = []

    def timedelta_milliseconds(self, td):
        if type(td[4]) is not datetime:
            return (td[0], td[1], td[2], td[3], td[4].days*86400000 + td[4].seconds*1000 + td[4].microseconds/1000)
        else:
            return (0, 0, 0, 0, 0)

    def calc_avr_speed(self, tmp):
        blocks_num, bytes_num, time = tmp[0], tmp[1], tmp[2]*1.0
        self.average_speed = (blocks_num/time, bytes_num/time)

    def write_data_to_file(self, file, arr):
        tmp = np.asarray(arr)
        df = pd.DataFrame(data=tmp)
        df['location_id'] = self.location_id
        with open(file, 'a') as f:
            df.to_csv(f, header=False, index=False)

    def print_stat(self, file):
        tmp = map(lambda x: self.timedelta_milliseconds(x), self.blocks_bytes_time_list) 
        #print '|||||||||||||||||||||||||'
        #print tmp
        #print '|||||||||||||||||||||||||'
        #print self.blocks_bytes_time_list
        #self.write_data_to_file(file, tmp)
        tmp = map(lambda x: (x[2], x[3], x[4]), tmp) 
        tmp = map(sum, zip(*tmp))
        self.calc_avr_speed(tmp)
        print self.location_id + ': ' + str(self.average_speed[0]) + ' blocks/millisec | ' + str(self.average_speed[1]) + ' bytes/millisec'


class ClusterFeatures:
    def __init__(self):
        self.cluster_list = []
        
    def print_stat(self):
        for i in self.cluster_list:
            i.print_stat('data.csv')

    def find_by_location_id(self, id):
        for it in range(len(self.cluster_list)):
            if (self.cluster_list[it].location_id == id):
                return it
        return False

    def blocks_number(self, s):
        e = re.compile(r" *(([0-9]+)\-([0-9]+)|[0-9]+),*")
        m = e.findall(s)
        res = 0
        for i in m:
            if not i[1]:
                res += 1
            else:
                res += int(i[2]) - int(i[1]) + 1
        return res

    def check_readingblobchunk(self, s):
        e = re.compile(r"(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2}) (?P<hour>\d{2}):(?P<minute>\d{2}):(?P<second>\d{2}),(?P<millisecond>\d*)\tD\tDataNode\t(?P<new_req>Started|Finished) reading blob chunk blocks \(BlockIds: (?P<blockId>[0-9a-f\-]+):(?P<blocks>[0-9\-]+), LocationId: (?P<LocationId>[a-z0-9]*), (BytesRead: (?P<BytesRead>[\d]*))?");
        m = e.search(s)
        if (m):
            new_req = True if (m.group('new_req') == "Started") else False
            bytesread = int(m.group('BytesRead')) if m.group('BytesRead') else None
            t = datetime(int(m.group('year')), int(m.group('month')), int(m.group('day')), int(m.group('hour')), int(m.group('minute')), int(m.group('second')), int(m.group('millisecond'))*1000)
            return (new_req, m.group('LocationId'), m.group('blockId'), m.group('blocks'), self.blocks_number(m.group('blocks')), bytesread, t)
        return False

    def add_new_line(self, s):
        res = self.check_readingblobchunk(s)
        if res is not False:
            it = self.find_by_location_id(res[1])
            if it is not False:
                if res[0]:
                    self.cluster_list[it].blocks_bytes_time_list += [(res[2], res[3], res[4], res[5], res[6])]
                else:
                    tmp = self.cluster_list[it].blocks_bytes_time_list[-1]
                    self.cluster_list[it].blocks_bytes_time_list[-1] = (tmp[0], tmp[1], tmp[2], res[5], res[6] - tmp[4])
            else: 
                if res[0]:
                    tmp = Location()
                    tmp.location_id = res[1]
                    tmp.chunk_id = res[2]
                    tmp.blocks_bytes_time_list += [(res[2], res[3], res[4], res[5], res[6])] # chunk, blocks, n_blocks, bytesread, time
                    self.cluster_list.append(tmp)
        #for i in self.cluster_list:
        #    print i.location_id
        #    print i.blocks_bytes_time_list 
        #print "###############################"


if __name__ == "__main__":
    if (len(sys.argv) != 2):
        print "Usage: 'get_features <LogFileName>'"
    else:
        with open(sys.argv[1]) as infile:
            cf = ClusterFeatures()
            for line in infile:
                cf.add_new_line(line)
            cf.print_stat()
