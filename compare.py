import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys


if __name__ == "__main__":
    if (len(sys.argv) != 3):
        print "Usage: 'compare.py <real_reqId-time.csv> <sim_reqId-time.csv>'"
    else:
        df_real = pd.read_csv(sys.argv[1], header=None, names=['reqId', 'real_time'])
        df_sim = pd.read_csv(sys.argv[2], header=None, names=['reqId', 'sim_time'])
        result = pd.merge(df_real, df_sim, on='reqId')
        print 'mean_real_time:', result['real_time'].mean()
        print 'mean_sim_time:', result['sim_time'].mean()
        result['delta'] = result['sim_time'] - result['real_time']
        al = result.shape[0]
        print al
        d0 = result[result['delta'] == 0].shape[0]
        d1 = result[result['delta'] == 1].shape[0]
        d2 = result[result['delta'] == 2].shape[0]
        dm1 = result[result['delta'] == -1].shape[0]
        dm2 = result[result['delta'] == -2].shape[0]
        print d0, d1, d2, dm1, dm2
        plt.hist(result['delta'], bins = 'auto')
        plt.xlim(-20, 20)
        plt.xlabel('sim_time - real_time, milliseconds')
        plt.title("Histogram of error")
        plt.show()