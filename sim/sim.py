#from bisect import insort
#import heapq
from Queue import Queue
from collections import defaultdict
from multiprocessing import Process
import random
import logging

from location_sim import LocationCharacteristic


class Event(object):
    def __init__(self, id):
        self.id = id

    def get_id(self):
        return self.id

class Request(Event):
    def __init__(self, id, chunk_id, blocks):
        super(Request, self).__init__(id)
        self.location_id = ""
        self.chunk_id = chunk_id
        self.blocks = blocks
        self.expected_blocks = self.blocks_number(blocks)
        self.completed_blocks = 0
        self.t_start = None

    def blocks_number(self, s):
        #e = re.compile(r" *(([0-9]+)\-([0-9]+)|[0-9]+),*")
        m = s # e.findall(s)
        res = 0
        for i in m:
            if not i[1]:
                res += 1
            else:
                res += int(i[1]) - int(i[0]) + 1
        return res

    def get_data(self):
        return (self.id, self.name, self.chunk_id, self.blocks)

    def on_start(self, now, scheduler, cluster):
        cluster.logger.info('Request id: %s time: %s ON START' % (self.id, now))
        self.t_start = now
        self.location_id = cluster.find_location(self.chunk_id)
        if self.location_id is not None:
            location = cluster.location_dict[self.location_id]
            for block_pair in self.blocks:
                for block in range(block_pair[0], block_pair[1] + 1):
                    location.read_block(now, scheduler, cluster, self.chunk_id, block, self.on_finish)

    def on_finish(self, now, scheduler, cluster, block):
        self.completed_blocks += 1
        cluster.logger.info('Request id: %s time: %s ON FINISH finished_block=%s completed_blocks=%s expected_blocks=%s' %\
         (self.id, now, block, self.completed_blocks, self.expected_blocks))
        if self.completed_blocks == self.expected_blocks:
            cluster.logger.info('Request id: %s time: %s READ FINISHED' % (self.id, now))
            cluster.add_request_data(self.id, now - self.t_start)
            return True
        return False


class Schedule(object):
    def __init__(self, cluster, id):
        self.id = id
        self.cluster = cluster
        self.events = defaultdict(list)
        self.now = 0
        self.cluster.logger.debug('Schedule id: %s CREATED' % (self.id))

    def add_event(self, time, event):    
        self.events[time].append(event)
        self.cluster.logger.debug('Schedule id: %s EVENT ADDED' % (self.id))

    def run(self):
        while self.events:
            while len(self.events[self.now]) > 0:
                call = self.events[self.now].pop(0)
                self.cluster.logger.debug('Schedule id: %s time: %s, EVENT CALLED' % (self.id, self.now))
                call(self.now, self, self.cluster)
            del self.events[self.now]
            self.now += 1


class LocationSim(object):
    def __init__(self, location_id):
        self.location_id = location_id
        self.read_queue = Queue()
        self.semaphore = 0

    def get_id(self):
        return self.location_id

    def read_block(self, now, scheduler, cluster, chunk, block, on_finish):
        dt = cluster.LocCharac.get_time(chunk, block)
        dt = int(dt + 0.5)
        self.read_queue.put((dt, lambda t, s, c: on_finish(t, s, c, block)))
        self.on_process_queue(now, scheduler, cluster)
        
    def on_process_queue(self, now, scheduler, cluster):
        if (not self.read_queue.empty()) and self.semaphore == 0:
            task = self.read_queue.get_nowait()
            cluster.logger.debug('LocationSim id: %s time: %s START READING' % (self.location_id, now))
            self.semaphore = 1
            scheduler.add_event(now + task[0], lambda t, s, c: self.on_read_completed(t, s, c, task[1]))

    def on_read_completed(self, now, scheduler, cluster, task):
        cluster.logger.debug('LocationSim id: %s time: %s FINISH READING' % (self.location_id, now))
        task(now, scheduler, cluster)
        self.semaphore = 0
        scheduler.add_event(now, lambda t, s, c: self.on_process_queue(t, s, c))


class ClusterSim(object):
    def __init__(self, id, location_dict, path_to_location_data):
        self.id = id
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)
        handler = logging.FileHandler('sim.log')
        handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

        self.request_data = {}
        self.location_dict = location_dict
        self.schedule = Schedule(self, id)
        self.LocCharac = LocationCharacteristic(path_to_location_data, self)
        self.logger.debug('ClusterSim id: %s CREATED' % (self.id))
            
    def add_request(self, request, time):
        self.schedule.add_event(time, request.on_start)

    def add_request_data(self, id, time):
        self.request_data[id] = time

    def find_location(self, chunk):
        return self.LocCharac.find_location(chunk)    

    def run(self):
        self.logger.info('Simulation starts')
        self.schedule.run()
        self.logger.info('Simulation finished')  