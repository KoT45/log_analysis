from sim import ClusterSim, LocationSim, Request
from datetime import datetime
from time import mktime
from collections import defaultdict
import sys
import re
import pandas as pd
import cProfile


flag = True
start = -1

def Check_GetBlockSet(s):
    global flag
    global start
    e = re.compile(r"GetBlockSet (?P<new_req><?->?)")
    m = e.search(s)
    if (m):
        new_req = True if (m.group('new_req') == "<-") else False
        if new_req:
            e = re.compile(r"(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2}) (?P<hour>\d{2}):(?P<minute>\d{2}):(?P<second>\d{2}),(?P<millisecond>\d*)\t")
            m = e.search(s)
            t = datetime(int(m.group('year')), int(m.group('month')), int(m.group('day')), int(m.group('hour')), int(m.group('minute')), int(m.group('second')), int(m.group('millisecond'))*1000)
            t = int(mktime(t.timetuple()) * 1000 + t.microsecond / 1000)
            if flag:
                start = t
                flag = False
            e = re.compile(r"RequestId: (?P<reqId>[a-f0-9\-]*),[, a-zA-Z:0-9\-\[\]]*BlockIds: (?P<blockId>[a-f0-9\-]*):\[(?P<blocks>[0-9,\- ]+)\][, a-zA-Z:0-9\-\[\]]*FetchFromCache: (?P<fromCache>True|False), FetchFromDisk: (?P<fromDisk>True|False)")
            m = e.search(s)
            if m and ((m.group('fromCache') == "True") and (m.group('fromDisk') == "True")):
                return (t, m.group('reqId'), m.group('blockId'), blocks_str_to_list(m.group('blocks')))
    return False

def Check_ReadingBlobChunk(s):
    e = re.compile(r"(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2}) (?P<hour>\d{2}):(?P<minute>\d{2}):(?P<second>\d{2}),(?P<millisecond>\d*)\tD\tDataNode\t(?P<new_req>Started|Finished) reading blob chunk blocks \(BlockIds: (?P<blockId>[0-9a-f\-]+):(?P<blocks>[0-9\-]+), LocationId: (?P<LocationId>[a-z0-9]*), (BytesRead: (?P<BytesRead>[\d]*))?");
    m = e.search(s)
    if (m) and (m.group('new_req') == "Started"):
        t = datetime(int(m.group('year')), int(m.group('month')), int(m.group('day')), int(m.group('hour')), int(m.group('minute')), int(m.group('second')), int(m.group('millisecond'))*1000)
        t = int(mktime(t.timetuple()) * 1000 + t.microsecond / 1000)
        return (t, m.group('LocationId'), m.group('blockId'), blocks_str_to_list(m.group('blocks')))
    return False

def blocks_str_to_list(s):
    e = re.compile(r" *(([0-9]+)\-([0-9]+)|[0-9]+),*")
    m = e.findall(s)
    res = []
    for i in m:
        if not i[1]:
            res += [(int(i[0]), int(i[0]))]
        else:
            res += [(int(i[1]), int(i[2]))]
    return res

requests = defaultdict(list)


def get_data(s):
    global requests
    tmp = Check_GetBlockSet(s)
    if tmp:
        t, reqId, chunk, blocks = tmp
        return t, reqId, chunk, blocks
    #else:
        #tmp = Check_ReadingBlobChunk(s)
        #if tmp:
            #location, chunk, blocks = tmp
    return None


if __name__ == "__main__":
    if (len(sys.argv) != 3):
        print "Usage: 'extract_features <LogFileName> <LocationData.csv>'"
    else: 
        store0 = LocationSim('store0')
        store1 = LocationSim('store1')
        store2 = LocationSim('store2')
        store3 = LocationSim('store3')
        store_dict = {'store0': store0, 'store1': store1, 'store2': store2, 'store3': store3}
        CS = ClusterSim(0, store_dict, sys.argv[2])
        with open(sys.argv[1]) as infile:
            for line in infile:
                tmp = get_data(line)
                if tmp:
                    #print tmp
                    time, reqId, chunk, blocks = tmp
                    r = Request(reqId, chunk, blocks)
                    CS.add_request(r, time - start)
        cProfile.run('CS.run()')

        df = pd.DataFrame(CS.request_data.items(), columns=['reqId', 'time'])
        with open('sim_reqId-time.csv', 'a') as f:
            df.to_csv(f, header=False, index=False)

    
