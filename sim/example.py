from collections import defaultdict

class Scheduler(object):
	def __init__(self):
		self.events = defaultdict(list)

	def run(self):
		t = 0
		while True:
			while len(self.events[t]) > 0:
				callable = self.events[t].shift()
				callable(t, self)
			del self.events[t]
			t += 1

	def schedule(self, t, callable):
		self.events[t].append(callable)


class Request(object):
	def __init__(self):
		self.expected_blocks = 10
		self.completed_blocks = 0
		pass # block id, size, etc

	def on_start(self, now, scheduler):
		print("t=%s start_req", now)
		for i in range(10):
			dt = random.randint(10)
			scheduler.schedule(now + dt, lambda t, s: self.on_finish(t, s, i))

	def on_finish(self, now, scheduler, block_index):
		self.completed_blocks += 1	
		print("t=%s finish_block %s", now, block_index)
		if self.completed_blocks == self.expected_blocks:
			print("t=%s finish_req", now)

class Disk(object):
	def __init__(self):
		self.read_queue = list()
		self.semaphore = 0
		# task = {.duration, .callback}

	def on_process_queue(self, now, scheduler):
		if len(self.read_queue) > 0 and self.semaphore == 0:
			task = self.read_queue.shift()
			self.semaphore = 1
			scheduler.schedule(now + task.duration, lambda t, s: self.on_read_completed(t, s, task))

	def on_read_completed(self, now, scheduler, task):
		print("t=%s read completed for task %s", task)
		task.callback()
		self.semaphore = 0
		scheduler.schedule(now, lambda t, s: self.on_process_queue(t, s))

	def read(self, scheduler, now, duration, callback):
		self.read_queue.append(Task(duration, callback))
		scheduler.schedule(now, lambda t, s: self.on_process_queue(t, s))

if __name__ == "__main__":
	s = Scheduler()
	r = Request()
	s.schedule(1, lambda t, s: r.on_start(t, s))