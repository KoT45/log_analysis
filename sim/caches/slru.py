from collections import OrderedDict

class SLRU:
    def __init__(self, protected_size, probationary_size):
        self.MAX_protected_size = protected_size
        self.MAX_probationary_size = probationary_size

        self.protected_size = 0
        self.probationary_size = 0

        self.protected_segment = OrderedDict()
        self.probationary_segment = OrderedDict()

    def free_protected(self):
        while self.protected_size > self.MAX_protected_size:
            key, size = self.protected_segment.popitem(last=False)
            self.protected_size -= size
            self.set(key, size)

    def free_probationary(self):
        while self.probationary_size > self.MAX_probationary_size:
            key, size = self.probationary_segment.popitem(last=False)
            self.probationary_size -= size

    def set(self, key, size):
        self.probationary_segment[key] = (key, size)
        self.probationary_size += size
        self.free_probationary()

    def get(self, key):
        if key in self.protected_segment.keys():
            key, size = self.protected_segment.pop(key)
            self.protected_segment[key] = (key, size)
            return (key, size)
        elif key in self.probationary_segment.keys():
            key, size = self.probationary_segment.pop(key)
            self.probationary_size -= size
            self.protected_segment[key] = (key, size)
            self.protected_size += size
            self.free_protected()
            return (key, size)
        else:
            return None
