import numpy as np
import pandas as pd
import random

class LocationCharacteristic:
    def __init__(self, path_to_data, cluster):
        self.cluster = cluster
        try:
            self.df = pd.read_csv(path_to_data, header=None, names=['chunk', 'chunk-block', 'time', 'location'])
            self.for_time = self.df.set_index('chunk-block')['time'].to_dict()
            self.for_location = self.df.set_index('chunk')['location'].to_dict()
        except Exception as e:
            self.cluster.logger.error('LocationCharacteristic read_csv ERROR')
            raise 

    def get_time(self, chunk_id, block):
        try:
            key = chunk_id + '_' + str(block)
            t = self.for_time[key]
            self.cluster.logger.debug("LocationCharacteristic get_time for chunk id:%s block:%s" % (chunk_id, block))
            return t
        except Exception as e:
            self.cluster.logger.error("LocationCharacteristic get_time ERROR can't find chunk id:%s block:%s! Return randint(0, 1)" % (chunk_id, block))
            return random.uniform(0, 2)

    def find_location(self, chunk_id):
        try:
            l = self.for_location[chunk_id]
            self.cluster.logger.debug("LocationCharacteristic find_location find location for chunk id:%s" % (chunk_id))
            return l
        except Exception as e:
            self.cluster.logger.error("LocationCharacteristic find_location ERROR can't find location for chunk id:%s!" % (chunk_id))
            return None

