import numpy as np
import pandas as pd
from collections import defaultdict
import sys
import re

class Blocks(object):
    def __init__(self):
        self.blocks_dict = defaultdict(list)  # counter, sum_time

    def add_block(self, id, time):
        tmp = self.blocks_dict[id]
        if tmp:
            self.blocks_dict[id] = [tmp[0] + 1, tmp[1] + time]
        else:
            self.blocks_dict[id] = [1, time]

    def calc_speed(self):
        for id in self.blocks_dict.keys():
            tmp = self.blocks_dict[id]
            self.blocks_dict[id] = tmp[1] / tmp[0]


def blocks_str_to_list_and_number(s):
    e = re.compile(r" *(([0-9]+)\-([0-9]+)|[0-9]+),*")
    m = e.findall(s)
    res = []
    n = 0
    for i in m:
        if not i[1]:
            res += [(int(i[0]), int(i[0]))]
            n += 1
        else:
            res += [(int(i[1]), int(i[2]))]
            n += int(i[2]) - int(i[1]) + 1
    return res, n


if __name__ == "__main__":
    if (len(sys.argv) != 2):
        print "Usage: 'get_block-speed <csvFileName>'"
    else:
        data = Blocks()
        df = pd.read_csv(sys.argv[1], header=None, names=['chunk', 'blocks', 'n_blocks', 'bytesread', 'time', 'location'])
        for index, row in df.iterrows():
            tmp, n = blocks_str_to_list_and_number(row['blocks'])
            t = row['time'] / (n *1.0)
            for i in tmp:
                for id in range(i[0], i[1] + 1):
                    data.add_block((row['location'], row['chunk'], id), t)
        data.calc_speed()
        
        df = pd.DataFrame(data.blocks_dict.items(), columns=['location-chunk-block', 'time'])
        df['chunk'] = df['location-chunk-block'].apply(lambda x: x[1])
        df['location'] = df['location-chunk-block'].apply(lambda x: x[0])
        df['chunk-block'] = df['location-chunk-block'].apply(lambda x: x[1] + '_' + str(x[2]))
        del df['location-chunk-block']
        df = df[['chunk', 'chunk-block', 'time', 'location']]
        
        with open('block-speed_data.csv', 'a') as f:
            df.to_csv(f, header=False, index=False)

