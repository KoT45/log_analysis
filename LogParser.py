import re
import sys
import pandas as pd

class HitRatioParser:
    hits = 0
    all = 0

    class Req:
        reqId = ""
        chunkId = ""
        blocks = []
        hits = 0
    reqhits = set()
    reqlist = []
    def Check_blocks(self, b1, b2, reqId):
        res = 0
        hit = False
        for i in b1:
            for j in b2:
                if (not ((i[1] < j[0]) or (i[0] > j[1]))):
                    tmp = (max(i[0], j[0]), min(i[1],j[1]))
                    res += tmp[1] - tmp[0] + 1
                else:
                    hit = True
        if hit: self.reqhits.add(reqId)
        return res

    def Find_By_BlockId(self, chunkId, blocks):
        res = []
        for it in range(len(self.reqlist)):
            if (self.reqlist[it].chunkId == chunkId):
                misses = self.Check_blocks(self.reqlist[it].blocks, blocks, self.reqlist[it].reqId)
                res += [(it, misses)]
        return tuple(res)

    def Find_By_ReqId(self, reqId):
        for it in range(len(self.reqlist)):
            if (self.reqlist[it].reqId == reqId):
                return it
        return False

    def blocks_str_to_list(self, s):
        e = re.compile(r" *(([0-9]+)\-([0-9]+)|[0-9]+),*")
        m = e.findall(s)
        res = []
        for i in m:
            if not i[1]:
                res += [(int(i[0]), int(i[0]))]
            else:
                res += [(int(i[1]), int(i[2]))]
        return res
        
    def Check_GetBlockSet(self, s):
        e = re.compile(r"GetBlockSet (?P<new_req><?->?)")
        m = e.search(s)
        if (m):
            new_req = True if (m.group('new_req') == "<-") else False
            if new_req:
                e = re.compile(r"RequestId: (?P<reqId>[a-f0-9\-]*),[, a-zA-Z:0-9\-\[\]]*BlockIds: (?P<blockId>[a-f0-9\-]*):\[(?P<blocks>[0-9,\- ]+)\][, a-zA-Z:0-9\-\[\]]*FetchFromCache: (?P<fromCache>True|False), FetchFromDisk: (?P<fromDisk>True|False)")
                m = e.search(s)
                if m and ((m.group('fromCache') == "True") and (m.group('fromDisk') == "True")):
                    return (new_req, m.group('reqId'), m.group('blockId'), self.blocks_str_to_list(m.group('blocks')))
            else:
                e = re.compile(r"RequestId: (?P<reqId>[a-f0-9\-]*),")
                m = e.search(s)
                return (new_req, m.group('reqId'), None, None)                
        return False
    
    def Check_ReadingBlobChunk(self, s):
        e = re.compile(r"(?P<new_req>Started|Finished) reading blob chunk blocks \(BlockIds: (?P<blockId>[0-9a-f\-]+):(?P<blocks>[0-9\-]+)");
        m = e.search(s)
        if (m):
            new_req = True if (m.group('new_req') == "Started") else False
            return (new_req, m.group('blockId'), self.blocks_str_to_list(m.group('blocks')))
        return False


    def check_new_line(self, s):
        reqId = ""
        blockId = ""
        blocks = ""
        new_req = True
        check_res = self.Check_GetBlockSet(s)
        if (check_res):
            if (check_res[0]):
                tmp = self.Req()
                tmp.reqId = check_res[1]
                tmp.chunkId = check_res[2]
                tmp.blocks = check_res[3]
                thits = 0
                for t in check_res[3]:
                    thits += t[1] - t[0] + 1
                tmp.hits = thits
                self.all += thits
                self.reqlist.append(tmp)
            else:
                reqIdIt = self.Find_By_ReqId(check_res[1])
                if (reqIdIt is not False):
                    self.hits += self.reqlist[reqIdIt].hits
                    del self.reqlist[reqIdIt]

        else:
            check_res = self.Check_ReadingBlobChunk(s)
            if (check_res):
                if (check_res[0]):
                    reqIdIts = self.Find_By_BlockId(check_res[1], check_res[2])
                    for it in reqIdIts:
                        self.reqlist[it[0]].hits -= it[1]
                    
        return True
        

    def print_stat(self):
        if (all != 0):
            print "all: " + str(self.all) + ", hits: " + str(self.hits) + ", hits ratio: " + str((self.hits/(self.all*1.0))*100) + "%"
        else:
            print "all: " + str(self.all) + ", hits: " + str(self.hits)
        df = pd.DataFrame(list(self.reqhits), columns=['reqId'])
        with open('requests_with_hits.csv', 'a') as f:
            df.to_csv(f, header=False, index=False)
      




if __name__ == "__main__":
    if (len(sys.argv) != 2):
        print "Usage: 'LogParser <LogFileName>'"
    else:
        with open(sys.argv[1]) as infile:
            hrp = HitRatioParser()
            for line in infile:
                hrp.check_new_line(line)
            hrp.print_stat()
