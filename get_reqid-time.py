from time import mktime
from datetime import datetime
from collections import defaultdict
import sys
import re
import numpy as np
import pandas as pd

class Request(object):
    def __init__(self):
        self.req_dict = defaultdict(list)  # reqId-time

    def Check_GetBlockSet(self, s):
        e = re.compile(r"(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2}) (?P<hour>\d{2}):(?P<minute>\d{2}):(?P<second>\d{2}),(?P<millisecond>\d*)\tD\tDataNode\tGetBlockSet (?P<new_req><?->?) RequestId: (?P<reqId>[a-f0-9\-]*)")
        m = e.search(s)
        if (m):
            t = datetime(int(m.group('year')), int(m.group('month')), int(m.group('day')), int(m.group('hour')), int(m.group('minute')), int(m.group('second')), int(m.group('millisecond'))*1000)
            new_req = True if (m.group('new_req') == "<-") else False
            if new_req:
                e = re.compile(r"FetchFromCache: (?P<fromCache>True|False), FetchFromDisk: (?P<fromDisk>True|False)")
                tmp = e.search(s)
                if tmp and ((tmp.group('fromCache') == "True") and (tmp.group('fromDisk') == "True")):
                    self.req_dict[m.group('reqId')] = t
            else:
                if self.req_dict.get(m.group('reqId')) is not None:
                    self.req_dict[m.group('reqId')] = t - self.req_dict[m.group('reqId')]        

    def timedelta_milliseconds(self, td):
        if type(td) is not datetime:
            return td.days*86400000 + td.seconds*1000 + td.microseconds/1000
        else:
            None

    def write_to_csv(self, file):
        for key in self.req_dict.keys():
            tmp = self.timedelta_milliseconds(self.req_dict[key])
            if tmp is not None:
                self.req_dict[key] = tmp
            else:
                del self.req_dict[key]
        df = pd.DataFrame(self.req_dict.items(), columns=['reqId', 'time'])
        with open(file, 'a') as f:
            df.to_csv(f, header=False, index=False)




if __name__ == "__main__":
    if (len(sys.argv) != 2):
        print "Usage: 'get_reqid-time <LogFileName>'"
    else:
        with open(sys.argv[1]) as infile:
            r = Request()
            for line in infile:
                r.Check_GetBlockSet(line)
            r.write_to_csv('reqId-time_data.csv')
